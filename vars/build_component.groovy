def call(Map pipelineParams) {
    
    // Set variables based on job trigger origin
    def triggerCause = currentBuild.getBuildCauses() // get the cause of the trigger
    def shortDescription = triggerCause['shortDescription'][0] // Strin value of the trigger cause 
    
    // General variables for use by multiple stages
    // ----------------------------------------------
    def build_dir = 'rwtg'
    def branch_master = 'master'
    def branch_develop = 'develop'
    def pkg_name = 'NXP_TRG'
    def artifactory_base_path = 'Segway/NXP_Terragraph/OS'
    
    // Set time stamp
    TimeZone.getTimeZone('UTC')
    Date date = new Date()
    String newdate = date.format("YYYYMMdd-HHmm")
    def timeStamp = '-' + newdate

    // Set package version string
    def pkg_version = env.BRANCH_NAME + timeStamp

    def archive_path = "$build_dir/build/tmp/deploy/images/ls1046adcbcn"
    def archive_filename = "${pkg_name}-${pkg_version}.tgz"

    // NOTE: There are comments at the end of the pipeline script

    // Start Pipeline
    // ----------------------------------------------
    pipeline {

        agent {
          node {
            label 'OS_BUILD'
          }
        }

        stages {
            stage('Pull (repo)') {
                steps {
                    script {
                        // PRINT ALL TYPES OF VERIABLES
                        echo "===========  START printParams()  ================"
                        def printParams = {
                            env.getEnvironment().each { name, value -> println "$name=$value" }
                        }
                        printParams()
                        echo "===========  END printParams()  ================"

                        echo "===========  START sh 'printenv'  ================"
                        sh 'printenv'
                        echo "===========  END sh 'printenv'  ================"

                        echo "===========  START echo sh(script: 'env|sort', returnStdout: true)  ================"
                        echo sh(script: 'env|sort', returnStdout: true)
                        echo "===========  END echo sh(script: 'env|sort', returnStdout: true)  ================"

                        echo "===========  START echo sh(returnStdout: true, script: 'env')  ================"
                        echo sh(returnStdout: true, script: 'env')
                        echo "===========  END sh echo sh(returnStdout: true, script: 'env')  ================"

                        echo "===========  START sh 'env'  ================"
                        sh 'env'
                        echo "===========  END sh 'env'  ================"
                        
                        echo "------------------------------"
                        //echo "triggerType=$triggerType"
                        //echo "eventType=$eventType"
                        //echo "refId=$refId"
                        echo "GIT_COMMIT=$GIT_COMMIT"
                        echo "triggerCause=$triggerCause"
                        echo "shortDescription=$shortDescription"
                        echo "------------------------------"

                        //===================================================
                        // ACTUAL START OF 'Pull (repo)' STAGE
                        //===================================================

                        // Create build folder and init manifest repo.
                        sh "mkdir -p $build_dir && cd $build_dir"
                        if (env.BRANCH_NAME == branch_master) {
                            sh "repo init -u ssh://git@bitbucket:7999/trgnxp/manifest.git -b $branch_master"   
                        }
                        else {
                            sh "repo init -u ssh://git@bitbucket:7999/trgnxp/manifest.git -b $branch_develop"
                        }
                        
                        // If a pull request is made and at the same time the develop branch has commits that where not merged into the
                        // branch that originated the pull request, Jenkins will perform a merge of the two branches before the first stage
                        // of the pipeline will start. This results in a new merge commit hash that exists only in the local git in the workspace.
                        // This issue will cause repo sync to fail. To resolve it, we need to run repo sync differently.
                        // We check if a merge was done by jenkins
                        author_name = sh([script: "git log -1 --pretty=format:'%an'", returnStdout: true]).trim()
                        commit_message_start = sh([script: "git log -1 --pretty=%B | cut -f 1 -d ' '", returnStdout: true]).trim()
                        if (author_name == 'Jenkins' && commit_message_start == 'Merge') {
                            // Merge was done by jenkins
                            sh """
                                cd $build_dir && mkdir -p sources/meta-radwin && cd sources/meta-radwin
                                git clone $WORKSPACE .
                                cd $WORKSPACE/$build_dir/
                                ln -s sources/meta-radwin/conf/setup-env.sh setup-env
                                repo sync poky meta-openembedded meta-freescale trgf/private_terragraph.puma trgnxp/downloads.git trgnxp/sstate-cache.git
                            """
                        }
                        else {
                            sh """
                                cd $build_dir
                                xmlstarlet ed --inplace -u \"/manifest/project[@name=\'trgnxp/meta-radwin.git\']/@revision\" -v $GIT_COMMIT .repo/manifest.xml
                                repo sync
                            """
                        }
                    }
                }
            }  // End stage Pull
            stage('Build (bitbake)') {
                steps {
                    script {
                        // Enter build environment and build the image
                        // . setup-env has to run in bash, NOT sh, hence the: #!/bin/bash
                        sh """#!/bin/bash
                            cd $build_dir
                            DEVELOP=yes . setup-env
                            bitbake tg-image
                        """
                    }
                }
            } // End stage Build
            stage('Package (Archive)') {
                steps {
                    script {
                        // Create a list of files and archive them
                        def filenames_list = 'archive_list.txt'
                        dir("$WORKSPACE/$archive_path") {
                            sh """
                                ls tg-qspi-bootimage-ls1046adcbcn-*.bin > $filenames_list
                                ls tg-qspi-uboot-ls1046adcbcn-*.tgu >> $filenames_list
                                ls tg-sysimage-ls1046adcbcn-*.bin >> $filenames_list
                                ls tg-sysimage-ls1046adcbcn-*.tgu >> $filenames_list
                                ls modules--4.19-r0-ls1046adcbcn-*.tgz >> $filenames_list
                                ls Image--4.19-r0-ls1046adcbcn-*.bin >> $filenames_list
                                ls fitImage--4.19-r0-ls1046adcbcn-*.bin >> $filenames_list
                                ls fitImage-its--4.19-r0-ls1046adcbcn-*.its >> $filenames_list

                                tar -c -z -v -f ../../../../../../$archive_filename --files-from $filenames_list
                            """     
                        }
                    }
                }
            }  // End stage Package
            stage('Publish (Artifactory)') {
                steps {
                    script {
                        def upload_dir = "$artifactory_base_path/Snapshot/${env.BRANCH_NAME}/$pkg_version"
                        if ( env.BRANCH_NAME ==~ /^(Release-(.)*)/ ) {
                            upload_dir = "$artifactory_base_path/Release/$pkg_version"
                        }

                        def repo_server = Artifactory.server 'Segway.Artifactory'

                        def buildInfo = Artifactory.newBuildInfo()
                        buildInfo.name = pkg_name
                        buildInfo.number = pkg_version

                        def uploadSpec = """{
                            "files": [{
                                "pattern": "$archive_filename",
                                "target": "$upload_dir/"
                                }]
                        }"""

                        repo_server.upload(uploadSpec)
                        repo_server.publishBuildInfo(buildInfo)
                    }
                }
            } // End stage Publish
        }
        post {
            success {
                echo "Delete entire workspace"
                dir("$WORKSPACE") {
                        sh 'rm -rf * .[!.]*'  
                    }
            }
        }
    }
}


// GENERAL COMMENTS
// --------------------------------

// xmlstarlet syntax:
// Find elemnt <project>, under <manifest> with attribute 'name=trgnxp/meta-radwin.git'
// and change the value of attribute 'revision' into new git id
// xmlstarlet ed --inplace -u \"/manifest/project[@name=\'trgnxp/meta-radwin.git\']/@revision\" -v $GIT_COMMIT default.xml
