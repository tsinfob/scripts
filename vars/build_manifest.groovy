def call(Map pipelineParams) {
    def triggerType = ''

    // Set parameters for job
    properties([
        parameters([
            [$class: 'StringParameterDefinition',
                defaultValue: '["1.2.3.4", "5.6.7.8"]',
                description: '''IP of units to be tested''',
                name: 'testDevices'
            ],

            [$class: 'ChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT',
            description: '',
            filterLength: 1,
            filterable: false,
            name: 'releaseVersion',
            randomName: 'choice-parameter-2942835182747611',
            script:
                [$class: 'GroovyScript',
                fallbackScript:
                    [classpath: [],
                    sandbox: false,
                    script: ''
                    ],
                    script:
                    [classpath: [],
                    sandbox: false,
                    script:
                        'return [\'false:selected\', \'true\']'
                    ]
                ]
            ],

            [$class: 'CascadeChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT',
            description: '',
            filterLength: 1,
            filterable: false,
            name: 'gitBranch',
            randomName: 'choice-parameter-2930846449195033',
            referencedParameters: 'releaseVersion',
            script:
                [$class: 'GroovyScript',
                fallbackScript:
                    [classpath: [],
                    sandbox: false,
                    script: ''
                    ],
                    script:
                    [classpath: [],
                    sandbox: false,
                        script: '''
                            def gitURL = "ssh://git@bitbucket.radwin.ltd.com:7999/trgnxp/manifest_test.git"

                            def refType = "--heads"
                            if (releaseVersion.equals("true")) {
                                refType = "--tags"
                            }

                            def command = "git ls-remote --refs $refType $gitURL"
                            def proc = command.execute()
                            proc.waitFor()

                            if ( proc.exitValue() != 0 ) {
                            println "Error, ${proc.err.text}"
                            System.exit(-1)
                            }

                            def branches = proc.in.text.readLines().collect {
                                it.replaceAll(/[a-z0-9]*\\trefs\\/heads\\//, '').replaceAll(/[a-z0-9]*\\trefs\\/tags\\//, '')
                            }
                            return branches.reverse()
                        '''
                    ]
                ]
            ]
        ]),

        // Trigger for Jira release webhook
        pipelineTriggers([
            [$class: 'GenericTrigger',
                genericVariables: [[key: 'releaseTagValue', value: '$.versionName'], [key: 'branchNameValue', value: '$.branchName'],],
                causeString: 'Jira Manifest Release Trigger', token: 'manifestRelease',
                printContributedVariables: true, printPostContent: true,
                regexpFilterText: '$branchNameValue', regexpFilterExpression: BRANCH_NAME
            ]
        ])
    ])
    
    // Set variables based on job trigger origin
    def checkoutPath = "refs/heads/$gitBranch"
    def triggerCause = currentBuild.getBuildCauses() // get the cause of the trigger
    def shortDescription = triggerCause['shortDescription'][0] // Strin value of the trigger cause

    if (shortDescription.contains('Generic Cause')) {
        // Job was tiggered by Bitbucket.
        checkoutPath = "$refId"
        // Check if trigger was of type tag (e.g. new release tag)
        if (triggerType == 'TAG') {
            releaseVersion = true
            gitBranch = checkoutPath.minus('refs/tags/')
        }
        else {
            releaseVersion = false
            gitBranch = checkoutPath.minus('refs/heads/')
        }

    }

    // General variables
    // ----------------------------------------------
    def buildDir = 'rwtg'
    def pkgName = 'NXP_TRG'
    def artifactoryBasePath = 'Segway/NXP_Terragraph/OS'
    
    // Set time stamp
    TimeZone.getTimeZone('UTC')
    Date date = new Date()
    String newdate = date.format("YYYYMMdd-HHmm")
    def timeStamp = '-' + newdate

    // Set package version string
    def pkgVersion = gitBranch + timeStamp

    def archivePath = "$buildDir/build/tmp/deploy/images/ls1046adcbcn"
    def archiveFilename = "${pkgName}-${pkgVersion}.tgz"

    // Start Pipeline
    // ----------------------------------------------
    pipeline {

        agent {
          node {
            label 'OS_BUILD'
          }
        }
        //agent any

        options {
            disableConcurrentBuilds()
        }
        
        stages {
            stage('Pull (repo)') {
                steps {
                    script {
                        // Create build folder and start repo sync.
                        sh """
                            mkdir -p $buildDir && cd $buildDir
                            repo init -u ssh://git@bitbucket:7999/trgnxp/manifest_test.git -b $checkoutPath
                            repo sync
                            if [ "$releaseVersion" = true ] ; then
                                cd bincache/sstate-cache
                                rm -rf *
                            fi
                        """
                    }
                }
            }  // End stage Pull
            stage('Build (bitbake)') {
                steps {
                    script {
                        // Enter build environment and build the image
                        // . setup-env has to run in bash NOT sh, hence the: #!/bin/bash
                        sh """#!/bin/bash
                            cd $buildDir
                            DEVELOP=yes . setup-env
                            bitbake tg-image
                        """
                    }
                }
            } // End stage Build
            stage('Package (Archive)') {
                steps {
                    script {
                        // Create a list of files and archive them
                        def filenamesList = 'archive_list.txt'
                        dir("$WORKSPACE/$archivePath") {
                            sh """
                                ls fitImage > $filenamesList
                                ls fitImage--4.19-r0-ls1046adcbcn-*.bin >> $filenamesList
                                ls fitImage-its--4.19-r0-ls1046adcbcn-*.its >> $filenamesList
                                ls fitImage-its-ls1046adcbcn >> $filenamesList
                                ls fitImage-its-tg-initramfs-ls1046adcbcn--4.19-r0-ls1046adcbcn-*.its >> $filenamesList
                                ls fitImage-its-tg-initramfs-ls1046adcbcn-ls1046adcbcn >> $filenamesList
                                ls modules--4.19-r0-ls1046adcbcn-*.tgz >> $filenamesList
                                ls rw-ls1046a-dcbcn--4.19-r0-ls1046adcbcn-*.dtb >> $filenamesList
                                ls tg-qspi-bootimage-ls1046adcbcn.bin >> $filenamesList
                                ls tg-qspi-bootimage-ls1046adcbcn-*.bin >> $filenamesList
                                ls tg-qspi-uboot-ls1046adcbcn.tgu >> $filenamesList
                                ls tg-qspi-uboot-ls1046adcbcn-*.tgu >> $filenamesList
                                ls tg-sysimage-ls1046adcbcn.bin >> $filenamesList
                                ls tg-sysimage-ls1046adcbcn.tgu >> $filenamesList
                                ls tg-sysimage-ls1046adcbcn-*.bin >> $filenamesList
                                ls tg-sysimage-ls1046adcbcn-*.tgu >> $filenamesList

                                tar -c -z -v -f ../../../../../../$archiveFilename --files-from $filenamesList
                            """     
                        }
                    }
                }
            }  // End stage Package
            stage('Publish (Artifactory)') {
                steps {
                    script {
                        def uploadDir = "$artifactoryBasePath/Snapshot/${env.BRANCH_NAME}/$pkgVersion"

                        if( env.releaseVersion == "true" ) {
                            uploadDir = "$artifactoryBasePath/Release/$pkgVersion"
                        }

                        def repoServer = Artifactory.server 'Segway.Artifactory'

                        def buildInfo = Artifactory.newBuildInfo()
                        buildInfo.name = pkgName
                        buildInfo.number = pkgVersion

                        def uploadSpec = """{
                            "files": [{
                                "pattern": "$archiveFilename",
                                "target": "$uploadDir/"
                                }]
                        }"""

                        repoServer.upload(uploadSpec)
                        repoServer.publishBuildInfo(buildInfo)
                    }
                }
            } // End stage Publish
        }
        post {
            always {
                echo "Delete entire workspace"
                cleanWs()
            }
        }
    }
}

